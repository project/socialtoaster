
/*
 * Function that goes through all form fields of a civicrm profile 
 * form and makes an associative array of their names and values
 * Ignores hidden fields and submit buttons
 * Renames fields so that 
 */
function socialtoaster_civicrm_submit_lead_profile_form(formname) {

  var $inputs = $('#'+formname+' :input')

  var formvalues = {};
  $inputs.each(function() {
    if( this.type != 'hidden' && this.type != 'submit' ) {

    	fieldname = this.name.replace('submitted[', '').replace(']', '');
      fieldvalue = $(this).val();

      if( fieldname == "phone-Primary-1" ) {
        fieldname = "phone";
      } else if( fieldname == "email-Primary" ) {
        fieldname = "email";
      } else if( fieldname == "street_address-Primary" ) {
        fieldname = "address_1";
      } else if( fieldname == "supplemental_address_1-Primary" ) {
        fieldname = "address_2";
      } else if( fieldname == "city-Primary" ) {
        fieldname = "city";
      } else if( fieldname == "state_province-Primary" ) {
        fieldname = "state";
        fieldvalue = $('#state_province-Primary :selected').text()
      } else if( fieldname == "postal_code-Primary" ) {
        fieldname = "zip";
      } else if( fieldname == "country-Primary" ) {
        fieldname = "country";
        fieldvalue = $('#country-Primary :selected').text()
      }

      formvalues[fieldname] = escape(fieldvalue);
    }
  });

  if( formvalues['first_name'] != '' && formvalues['last_name'] != '' ) {
    formvalues['full_name'] = formvalues['first_name'] + " " + formvalues['last_name'];
  } else if( formvalues['first_name'] != '' ) {
    formvalues['full_name'] = formvalues['first_name'];
  } else if( formvalues['last_name'] != '' ) {
    formvalues['full_name'] = formvalues['last_name'];
  }  	

  delete formvalues['first_name'];
  delete formvalues['last_name'];
  
  st_promote_lead(formvalues, function(){ document.getElementsByName('_qf_Edit_next')[0].click(); });
  //st_promote_lead(formvalues, function() { document.getElementById(formname).submit() });
  return false;
}


/*
 * Function that goes through all form fields of a civicrm contribution 
 * form and makes an associative array of their names and values
 * Ignores hidden fields and submit buttons
 * Renames fields so that 
 */
function socialtoaster_civicrm_submit_lead_contribution_form(formname) {

  var $inputs = $('#'+formname+' :input')

  var formvalues = {};
  $inputs.each(function() {
    if( this.type != 'hidden' && this.type != 'submit' ) {

    	fieldname = this.name.replace('submitted[', '').replace(']', '');
      fieldvalue = $(this).val();

      if( fieldname == "phone-Primary-1" ) {
        fieldname = "phone";
      } else if( fieldname == "email-5" ) {
        fieldname = "email";
      } else if( fieldname == "billing_street_address-5" ) {
        fieldname = "address_1";
      } else if( fieldname == "billing_city-5" ) {
        fieldname = "city";
      } else if( fieldname == "billing_state_province_id-5" ) {
        fieldname = "state";
        fieldvalue = $('#billing_state_province_id-5 :selected').text()
      } else if( fieldname == "billing_postal_code-5" ) {
        fieldname = "zip";
      } else if( fieldname == "billing_country_id-5" ) {
        fieldname = "country";
        fieldvalue = $('#billing_country_id-5 :selected').text()
      }

      formvalues[fieldname] = escape(fieldvalue);
    }
  });

  if( formvalues['billing_first_name'] != '' && formvalues['billing_last_name'] != '' ) {
    formvalues['full_name'] = formvalues['billing_first_name'] + " " + formvalues['billing_last_name'];
  } else if( formvalues['billing_first_name'] != '' ) {
    formvalues['full_name'] = formvalues['billing_first_name'];
  } else if( formvalues['billing_last_name'] != '' ) {
    formvalues['full_name'] = formvalues['billing_last_name'];
  }  	

  delete formvalues['billing_first_name'];
  delete formvalues['billing_last_name'];
  delete formvalues['credit_card_type'];
  delete formvalues['credit_card_number'];
  delete formvalues['cvv2'];
  delete formvalues['credit_card_exp_date[M'];
  delete formvalues['credit_card_exp_date[Y'];
  
  st_promote_lead(formvalues, function(){ document.getElementsByName('_qf_Main_upload')[0].click(); });
  //st_promote_lead(formvalues, function() { document.getElementById(formname).submit() });
  return false;
}


