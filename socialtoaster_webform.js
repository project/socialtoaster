
/*
 * Function that goes through all webform form fields and makes an 
 * associative array of their names and values
 * Ignores hidden fields and submit buttons
 */
function socialtoaster_webform_submit_lead_form(formname) {

  var $inputs = $('#'+formname+' :input')

  var formvalues = {};
  $inputs.each(function() {
      if( this.type != 'hidden' && this.type != 'submit' ) {
      	fieldname = this.name.replace('submitted[', '').replace(']', '');

        if( fieldname == "name" ) {
          fieldname = "full_name";
        }

        formvalues[fieldname] = escape($(this).val());
      }
  });

  st_promote_lead(formvalues, function() { document.getElementById(formname).submit() });
  return false;
}


