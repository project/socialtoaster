
SocialToaster for Drupal
====================

This module allows drupal to integrate with SocialToaster. Users with adminstrative
access can turn on and off SocialToaster settings for specific node types.
A block with SocialToaster's Signup widget will become available in the blocks
administration page.  Ambassadors will be able to sign up to promote your content by
clicking the links in that block

SocialToaster is a revolutionary hosted platform that allows your supporters to automatically
drive traffic to your website via their social network accounts like Facebook & Twitter, while
providing you with advanced real-time marketing analytics. 

For more information about SocialToaster, please visit http://www.socialtoaster.com

INSTALLATION
------------

1. Extract the SocialToaster module to your modules directory
       sites/all/modules


CONFIGURATION
-------------
   
1. Enable SocialToaster module in:
       admin/build/modules

2. If you would like to integrate with the CiviCRM or Webform modules, make sure
   they are enabled first, and then enable the appropriate SocialToaster integration
   module.

2. Contact SocialToaster at 410.889.7770 or sales@socialtoaster.com
   to obtain your socialtoaster keys

3. You'll now find a SocialToaster link in the Site Configuration menu
       admin/settings/socialtoaster

4. Choose the node types that you would like to share through SocialToaster
   Under advanced settings, enter the keys into the SocialToaster settings. 
   The rest of the advanced settings should stay the same.

5. Configure the permissions for the SocialToaster module so that anyone creating
   content on your site that is going to be shared through socialtoaster has permissin
   to "administer SocialToaster".
   
6. Add the "SocialToaster Ambassador Signup" block to a region in the blocks administration
   page.
       admin/build/block

7. That's it!  Now when someone comes to your site they will see the signup widget in the
   block region that you added the block to.  When you create content that has a type that
   was chosen in step 4, that content will be shared to your ambassadors' social networks.


WEBFORM INTEGRATION
-------------------

SocialToaster has a seperate module that uses the Webform Module to capture leads.
It automatically adds a snippet of javascript code to every webform node and sends
the form information to SocialToaster's server.  In order for the information to get
stored correctly, the name field must be called "Name" and the email field must be
called "Email".  There are plans to make this more flexible in the future.


CIVICRM INTEGRATION
-------------------
SocialToaster has a seperate module that uses the CiviCRM Module to capture leads.
It automatically adds a snippet of javascript code to civicrm profile and contribution
forms, which sends information to SocialToaster's server.  It only currently sends
primary address and billing information.  There are plans to make this more flexible
in the future.
