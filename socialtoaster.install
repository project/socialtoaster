<?php

/**
 * @file
 * The install file for SocialToaster allows the module to install (and uninstall) itself. This is required as this module uses its own table.
 */

/**
 * Implementation of hook_install().
 */
function socialtoaster_install() {
  drupal_install_schema('socialtoaster');
}


/**
 * Implementation of hook_schema().
 */
function socialtoaster_schema() {

  $schema['socialtoaster_node'] = array(
    'fields'                      => array(
      'vnid'                        => array('type' => 'serial', 'unsigned' => TRUE,  'not null' => TRUE),
      'nid'                         => array('type' => 'int', 'unsigned' => TRUE,  'not null' => TRUE, 'default' => 0),
      'socialtoaster_short_summary' => array('type' => 'varchar', 'length' => 110, 'not null' => TRUE, 'default' => ''),
      'socialtoaster_label'         => array('type' => 'varchar', 'length' => 110, 'not null' => TRUE, 'default' => ''),
      'socialtoaster_post'          => array('type' => 'tinyint',  'not null' => TRUE, 'default' => 0),
      'socialtoaster_date_posted'   => array('type' => 'datetime', 'not null' => TRUE),
    ),
    'primary key' => array('vnid'),
  );

$schema['socialtoaster_node'] = array(
  'description' => t('A table that stores a node\'s socialtoaster information'),
  'fields' => array(
    'vnid' => array(
      'description' => t('The primary key of this table.'),
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'nid' => array(
      'description' => t('The node id for the node that relates to this information.'),
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'socialtoaster_short_summary' => array(
      'description' => t('The short summary that gets passed to the social media sites.'),
      'type' => 'varchar',
      'length' => '110',
      'not null' => TRUE,
    ),
    'socialtoaster_label' => array(
      'description' => t('The label that shows in the SocialToaster reports.'),
      'type' => 'varchar',
      'length' => '110',
      'not null' => TRUE,
    ),
    'socialtoaster_post' => array(
      'description' => t('The value that controls whether or not the node gets posted to SocialToaster.'),
      'type' => 'int',
      'unsigned' => TRUE,
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    ),
    'socialtoaster_date_posted' => array(
      'description' => t('The date and time that the node was posted to SocialToaster.'),
      'type' => 'datetime',
      'not null' => TRUE,
      'default' => '0000-00-00 00:00:00',
    ),
  ),
  'primary key' => array('vnid'),
);


  return $schema;

}

/**
 * Implementation of hook_uninstall().
 */
function socialtoaster_uninstall() {
  drupal_uninstall_schema('socialtoaster');

  // Clear the node specific variables
  $types = node_get_types('names');
  foreach ($types as $type => $name) {
    variable_del('socialtoaster_type_' . $type );
  }
  
  variable_del('socialtoaster_name');
  variable_del('socialtoaster_post_default');
  variable_del('socialtoaster_key');
  variable_del('socialtoaster_secret');
  variable_del('socialtoaster_domain');
  variable_del('socialtoaster_path_share');
  variable_del('socialtoaster_js_api_domain');
  variable_del('socialtoaster_js_inline_domain');
  variable_del('socialtoaster_path_nonce');
  variable_del('socialtoaster_debug');
}
