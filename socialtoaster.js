
// Function needed to include javascript file from SocialToaster server
function js_include(filename) {
    document.write('<' + 'script');
    document.write(' language="javascript"');
    document.write(' type="text/javascript"');
    document.write(' defer="defer"');
    document.write(' src="' + filename + '">');
    document.write('</' + 'script' + '>');
}
